var from = {row:-1, col:-1};   // variables de control para
var to = {row:-1, col:-1};     // los movimientos.

var player=1; // 1-humano , 0-maquina

//guarda la ruta realizada para jugar
var move_path = new Array();

//array con las piezas que se come en una jugada
var remove_stones = new Array();

img_white_stone = "./img/whiteStone.png"
img_black_stone = "./img/blackStone.png"
img_fondo_claro = "./img/fondoBlanco.png"
img_fondo_oscuro ="./img/fondoOscuro.png"

var board = [[0,1,0,1,0,1,0,1],
             [1,0,1,0,1,0,1,0],
             [0,1,0,1,0,1,0,1],
             [0,0,0,0,0,0,0,0],
             [0,0,0,0,0,0,0,0],
             [2,0,2,0,2,0,2,0],
             [0,2,0,2,0,2,0,2],
             [2,0,2,0,2,0,2,0]];

//ejecuta un movimiento del humano
function move(){
  //console.log(move_path);
  var last = move_path.length-1;
  board[move_path[0][0]][ move_path[0][1]]=0;
  if(player){
    board[move_path[last][0]][move_path[last][1]]=2;
  }//else{
    //board[move_path[last][0]][move_path[last][1]]=1;
  //}
  for(var i=0; i < remove_stones.length; i++)
    board[remove_stones[i][0]][remove_stones[i][1]]=0;
  print_board(board);
  from.row = -1;
  from.col = -1;
  to.row = -1;
  to.col = -1;
  var a = {row: -1, col:1}, b = {row:-1,col:-1};
  var pos = {row: move_path[last][0], col: move_path[last][1]};
  if(player && (!((can_eat(board,pos,a)||can_eat(board,pos,b)) && remove_stones.length > 0) )){
    player = 0;
    jugar_maquina();
  }//else{
  remove_stones=[];
  move_path=[];
    //player=1;
  //}
}

function eval(tab){
  var j1 = 0,j2 = 0;
  for(var i in tab)
    for(var j in i){
      if(j == 1) j1 += 1;
      if(j == 2) j2 += 1;
    }
  if(j1 == 0);
    return 500;
  if(j2 == 0);
    return -500;
  return (j2/j1);
}

//evalua si la celda esta vacia
function is_empty(cell,xboard){
  if(xboard[cell.row][cell.col] == 0) return true;
  return false;
}

//evalua si el movimiento es diagonal y valido
//en caso de que coma alguna ficha agrega esta posicion
//a remove_stones si es jugador humano.
function is_diagonal_ok(from,to,xboard){
  if(!pos_is_ok(from) || !pos_is_ok(to)) return 0;
  //diagonal jugador humano
  if( player && (from.row > to.row) && (Math.abs(from.row - to.row) == Math.abs(from.col - to.col)) ){
    if(Math.abs(from.row - to.row) == 2){
      (from.col < to.col)? c = from.col+1 : c = from.col-1;
      if(xboard[from.row-1][c] != 1)
        return 0;
      else
        remove_stones.push([from.row-1,c]);
    }
    return 1;
  }
  //diagonal maquina
  if(!player && (from.row < to.row) && (Math.abs(from.row - to.row) == Math.abs(from.col - to.col))){
    return 1;
  }
  return 0;
}

// evalua si pos esta dentro del rango 8x8
function pos_is_ok(pos){
  if(pos.row >= 0 && pos.row <= 7 && pos.col >= 0 && pos.col <= 7) return true;
  return false;
}

// evalua si el movimiento from -> to es valido
// from={row,col} to={row,col}
function move_is_ok(from,to, xboard){
  if(pos_is_ok(to) && is_empty(to,xboard) && is_diagonal_ok(from,to,xboard)){
    return true;
  }
  return false;
}

//funcion para manejar solo las jugadas del humano
function add_move(cell){
  var q = parseInt(cell.id);
  var row = Math.floor(q/8);
  var col = q % 8;
  if(player){
    if(from.row == -1){
      if(board[row][col] == 2){
        from.row = row;
        from.col = col;
      }else{
        alert("debe seleccionar la piedra que va a mover!");
      }
    }
    else{
      to.row = row;
      to.col = col;
      if(move_is_ok(from,to,board)){
        if(move_path.length==0)move_path.push([from.row,from.col]);
        move_path.push([to.row,to.col]);
        //console.log(move_path);
        //console.log("move is ok!");
      }else{
        remove_stones=[];
        //console.log("movimiento invalido");

      }
    }
  }
}

function debg(x){
  for(var r = 0; r < 8; r++){
    console.log(board[r]);
  }
}

//imprime un tablero x en html;
function print_board(x){
  var i=0;
  for(var r=0; r<8; r++,i++){
    for(var c=0; c<8; c++,i++){
      id=(r*8)+c;
      var td = document.getElementById(id);
      if(x[r][c]==0){
        if(i%2==0) td.innerHTML='<img src='+img_fondo_claro+'>';
        else td.innerHTML='<img src='+img_fondo_oscuro+'>';
      }
      if(x[r][c]==1){
        td.innerHTML='<img src='+img_black_stone+'>';
      }
      if(x[r][c]==2){
        td.innerHTML='<img src='+img_white_stone+'>';
      }
    }
    console.log(x[r]);
  }
}


//load board at fisrt time
function load_board(x){
  var i=0;
  for(var r=0; r<8; r++,i++){
    document.write('<tr>');
    for(var c=0; c<8; c++,i++){
      id=(r*8)+c;            //calculate cell id
      document.write('<td id='+id+' onclick=\"add_move(this);\">');
      if(x[r][c]==0){
        if(i%2==0)document.write('<img src="img/fondoBlanco.png" name='+img_fondo_claro+'></td>');
        else document.write('<img src="img/fondoOscuro.png" name='+img_fondo_oscuro+'></td>');
      }
      if(x[r][c]==1){
        document.write('<img src="img/blackStone.png"></td>');
      }
      if(x[r][c]==2){
        document.write('<img src="img/whiteStone.png"></td>');
      }
    }
    document.write('</tr>');
  }
}

//xor para ahorrar codigo
function xor(a,b){
  return (a||!b)&&(!a||b);
}

// evalua si pos es enemigo
function is_enemy(pos,xboard){
  if(xboard[pos.row][pos.col]-1 == (1-player)) return 1;
  return 0;
}

//evalua si desde pos con una direccion se puede comer ficha
function can_eat(board,pos,dir){
  var pos2 = {row: pos.row+(2*dir.row), col: pos.col+(2*dir.col)};
  var enemy = {row: pos.row+dir.row, col: pos.col+dir.col};
  if(pos_is_ok(pos2) && is_empty(pos2,board) && is_enemy(enemy,board)){
    return true;
  }
  return false;
}

function canmove(nboard, pos, dir, mnmx){
  if(pos.row+dir.row < 0 || pos.row+dir.row >= nboard.length) return false;
  if(pos.col+dir.col < 0 || pos.col+dir.col >= nboard.length) return false;
  if(nboard[pos.row+dir.row][pos.col+dir.col] == 0) return true;
  if(nboard[pos.row+dir.row][pos.col+dir.col] != mnmx){
    if(pos.row+(dir.row*2) < 0 || pos.row+(dir.row*2) >= nboard.length) return false;
    if(pos.col+(dir.col*2) < 0 || pos.col+(dir.col*2) >= nboard.length) return false;
    if(nboard[pos.row+(dir.row*2)][pos.col+(dir.col*2)] == 0) return true;
  }
  return false;
}

//genera tableros, si se puede
function genboard(aboard,pos,dir){
  var nboard=new Array();
  //copia el board
  for(var i = 0; i < aboard.length; i++){
    //aboard[i] = new Array();
    nboard[i] = new Array();
    for(var j = 0; j < aboard.length; j++)
      nboard[i][j]= aboard[i][j];
  }
  //mueve la pieza si se puede
  //console.log("generando");
  if(nboard[pos.row+dir.row][pos.col+dir.col] == 0){
    nboard[pos.row+dir.row][pos.col+dir.col] = nboard[pos.row][pos.col];
    nboard[pos.row][pos.col] = 0;
  }else{
    var dir2={};
    //come fichas mientras pueda comer
    while(true && pos_is_ok(pos)){
      var tmp1={row: pos.row+dir.row, col: pos.col+dir.col};
      var tmp2={row: pos.row+(dir.row*2), col: pos.col+(dir.col*2)};
      if(!(pos_is_ok(tmp1) && pos_is_ok(tmp2))) break;
      nboard[tmp1.row][tmp1.col] = 0;
      nboard[tmp2.row][tmp2.col] = nboard[pos.row][pos.col];
      nboard[pos.row][pos.col] = 0;
      pos.row += (dir.row*2);
      pos.col += (dir.col*2);
      dir2.row = dir.row;
      dir2.col = dir.col*(-1);
      if((xor(can_eat(nboard,pos,dir),can_eat(nboard,pos,dir2)))) break;
      if(can_eat(nboard,pos,dir2)) dir = dir2;
    }
  }
  return nboard;
}

//checkea el minimo o maximo 
function check(actson,genson,mnmx) {
  if(genson < actson && mnmx == 2) return true;
  if(genson > actson && mnmx == 1) return true;
  return false;
}

//pregunta si puede jugar y genera tableros mnmx determina 
//cual debe escoger, dependiendo del jugador 1 maquina 2 humano
function genmove(deep,mnmx,nboard){
  //console.log("juega " + player + " con " + deep);
  if(deep == 0) return nboard;
  var pos={},dir={},dir2={};
  dir.col = 1;
  dir2.col = -1;
  if(mnmx == 2) dir.row = -1;
  else dir.row = 1;
  dir2.row = dir.row;
  var actson,genson,actvalue,genvalue;
  for(var i = 0; i < 8; i++){
    for(var j = 0; j < 8; j++){
      if(nboard[i][j] == mnmx ){
        pos.row = i;
        pos.col = j;
        if(canmove(nboard,pos,dir,mnmx)){
          genson = genboard(nboard,pos,dir);
          genvalue = eval(genmove(deep-1,mnmx,genson));
          if(actson == undefined || check(actvalue,genvalue,mnmx)) {
            actvalue = genvalue;
            actson = genson;
          }
        }
        if(canmove(nboard,pos,dir2,mnmx)){
          genson = genboard(nboard,pos,dir2);
          genvalue = eval(genmove(deep-1,mnmx,genson));
          if(actson == undefined || check(actvalue,genvalue,mnmx)) {
            actvalue = genvalue;
            actson = genson;
          }
        }
      }
    }
  }
  if(actson == undefined) return nboard;
  //console.log("va a salir ", deep);
  //debg(actson);
  if(mnmx == 2) mnmx = 1;
  else mnmx = 2;
  nboard = actson;
  return nboard;
}


function jugar_maquina(){
  console.log("jugar maquina");
  var profundidad = 5;
  board = genmove(profundidad,1,board);
  print_board(board);
  player = 1;

  console.log("ya acabe");
}
//carga el tablero solo la primera vez
load_board(board);
